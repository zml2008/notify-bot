package com.zachsthings.notifybot.util;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.logging.Formatter;
import java.util.logging.LogRecord;

/**
 * @author zml2008
 */
public class LogFormatter extends Formatter {
    private final DateFormat format;

    public LogFormatter() {
        this(new SimpleDateFormat("HH:mm:ss"));
    }

    public LogFormatter(DateFormat format) {
        this.format = format;
    }

    @Override
    public String format(LogRecord record) {
        StringBuilder builder = new StringBuilder()
                .append('[')
                .append(format.format(new Date(record.getMillis())))
                .append("] [")
                .append(record.getLevel().getLocalizedName())
                .append("] ")
                .append(formatMessage(record))
                .append('\n');

        if (record.getThrown() != null) {
            StringWriter s = new StringWriter();
            record.getThrown().printStackTrace(new PrintWriter(s));
            builder.append(s.getBuffer());
        }

        return builder.toString();
    }
}
