package com.zachsthings.notifybot;

import com.zachsthings.notifybot.util.LogFormatter;
import org.pircbotx.PircBotX;
import org.pircbotx.User;
import org.pircbotx.exception.IrcException;

import java.io.File;
import java.io.IOException;
import java.util.logging.ConsoleHandler;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Main class
 */
public class BotController {
    private static final String DEFAULT_CONFIG_NAME = "config.json";
    private final Logger logger = Logger.getLogger(BotController.class.getCanonicalName());
    static {
        // Apply global logger settings
        Logger logger = Logger.getLogger(BotController.class.getPackage().getName());
        logger.setUseParentHandlers(false);
        ConsoleHandler handler = new ConsoleHandler();
        handler.setFormatter(new LogFormatter());
        logger.addHandler(handler);
    }
    private final BotConfiguration config;
    private final PircBotX bot;
    public BotController() {
        config = new BotConfiguration(new File(DEFAULT_CONFIG_NAME));
        bot = new PircBotX();
        bot.setAutoNickChange(true);
        bot.getListenerManager().addListener(new BotListener(this));
    }

    public BotConfiguration getConfig() {
        return config;
    }

    public boolean isAdmin(User user) {
        return config.isAdmin(user.getNick()) && user.isIdentified(); // TODO isIdentified() does not work -- look into this more
    }

    public void start() {
        config.load();
        try {
            bot.connect(config.getServer());
        } catch (IOException e) {
            logger.log(Level.SEVERE, "Could not connect to server " + config.getServer() + "!", e);
        } catch (IrcException e) {
            e.printStackTrace();
        }
        bot.changeNick(config.getNick());
        for (String channel : config.getChannels().keySet()) {
            bot.joinChannel(channel);
        }
        logger.info("Connected to " + config.getChannels().size() + " channels.");
    }

    public void stop() {
        bot.shutdown();
    }

    public Logger getLogger() {
        return logger;
    }

    public static void main(String[] args) {
        final BotController controller = new BotController();
        controller.start();
        Runtime.getRuntime().addShutdownHook(new Thread() {
            @Override
            public void run() {
                controller.stop();
            }
        });
    }
}
