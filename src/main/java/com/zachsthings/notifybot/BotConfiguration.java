package com.zachsthings.notifybot;

import com.google.gson.FieldNamingPolicy;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.Reader;
import java.io.Writer;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * @author zml2008
 */
public class BotConfiguration {
    private static final Logger LOGGER = Logger.getLogger(BotConfiguration.class.getCanonicalName());
    private final Gson gson;
    private File configFile;
    private ConfigValues config;

    public BotConfiguration(File file) {
        this.configFile = file;
        gson = new GsonBuilder()
                .setPrettyPrinting()
                .setFieldNamingStrategy(FieldNamingPolicy.LOWER_CASE_WITH_DASHES)
                .create();
    }

    private void createParent() {
        File parent = configFile.getParentFile();
        if (parent != null) {
            parent.mkdirs();
        }
    }

    public void load() {
        Reader reader = null;
        try {
            reader = new InputStreamReader(new FileInputStream(configFile), Charset.forName("UTF-8"));
            config = gson.fromJson(reader, ConfigValues.class);
        } catch (FileNotFoundException e) {
            createParent();
            try {
                initialize();
                configFile.createNewFile();
            } catch (IOException e1) {
                return;
            }
            config = new ConfigValues();
        } finally {
          if (reader != null) {
              try {
                  reader.close();
              } catch (IOException e) {
              }
          }
        }
    }

    public void save() {
        if (config == null) {
            load();
        }
        Writer writer = null;
        try {
            createParent();
            writer = new OutputStreamWriter(new FileOutputStream(configFile), Charset.forName("UTF-8"));
            gson.toJson(config, writer);
        } catch (FileNotFoundException ignore) {
        } finally {
            if (writer != null) {
                try {
                    writer.close();
                } catch (IOException e) {
                }
            }
        }
    }

    public void initialize() {
        if (!configFile.exists()) {
            InputStream is = BotConfiguration.class.getResourceAsStream("/" + configFile.getName() + ".def");
            if (is == null) {
                throw new IllegalStateException("Example config file does not exist!");
            }
            FileOutputStream os = null;
            byte[] buffer = new byte[8192];
            try {
                os = new FileOutputStream(configFile);
                while (is.read(buffer) != -1) {
                    os.write(buffer);
                }
                os.flush();
            } catch (IOException e) {
                LOGGER.log(Level.WARNING, "Error creating default configuration! Exiting!", e);
                System.exit(0);
            } finally {
                if (is != null) {
                    try {
                        is.close();
                    } catch (IOException ignore) {
                    }
                }

                if (os != null) {
                    try {
                        os.close();
                    } catch (IOException ignore) {
                    }
                }
            }
            LOGGER.info("Default configuration file created. Modify it for your preferred configuration.");
            System.exit(0);
        }
    }

    public ChannelInfo getChannel(String channelName) {
        return config.channels.get(channelName);
    }

    public Map<String, ChannelInfo> getChannels() {
        return Collections.unmodifiableMap(config.channels);
    }

    public boolean isAdmin(String user) {
        return config.admins.contains(user);
    }

    public String getServer() {
        return config.server;
    }

    public String getNick() {
        return config.nick;
    }

    private static class ConfigValues {
        private String server;
        private String nick;
        private List<String> admins = new ArrayList<String>();
        private Map<String, ChannelInfo> channels = new HashMap<String, ChannelInfo>();
    }

    public static class ChannelInfo {
        private String notifyMessage;

        public String getNotifyMessage() {
            return notifyMessage;
        }
    }
}
