package com.zachsthings.notifybot;

import org.pircbotx.PircBotX;
import org.pircbotx.hooks.ListenerAdapter;
import org.pircbotx.hooks.events.JoinEvent;
import org.pircbotx.hooks.events.MessageEvent;

/**
 * @author zml2008
 */
public class BotListener extends ListenerAdapter<PircBotX> {
    private static final String COMMAND_PREFIX = ".";
    private final BotController controller;

    public BotListener(BotController controller) {
        this.controller = controller;
    }

    @Override
    public void onJoin(JoinEvent<PircBotX> event) {
        if (event.getUser() != event.getBot().getUserBot()) {
            BotConfiguration.ChannelInfo channel = controller.getConfig().getChannel(event.getChannel().getName());

            if (channel != null && channel.getNotifyMessage() != null) {
                event.getBot().sendNotice(event.getUser(), channel.getNotifyMessage());
            }
        }
    }

    @Override
    public void onMessage(MessageEvent<PircBotX> event) {
        if (controller.isAdmin(event.getUser())) {
            String message = event.getMessage();
            if (message.startsWith(COMMAND_PREFIX)) {
                message = message.substring(COMMAND_PREFIX.length());
                String[] split = message.split(" ");
                if (split.length == 0) {
                    event.respond("No command specified!");
                }
                String command = split[0];
                if (command.equalsIgnoreCase("stop")) {
                    controller.stop();
                    event.respond("Stopping");
                } else if (command.equalsIgnoreCase("reload")) {
                    controller.getConfig().load();
                    event.respond("Reloaded");
                } else {
                    event.respond("Unknown command: " + command);
                }
            }
        }
    }
}
